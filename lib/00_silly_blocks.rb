def reverser
  yield.split.map { |word| word.reverse }.join(' ')
end

def adder(a=1)
  a + yield
end

def repeater(n=0)
  n == 0 ? yield : (0...n).each { |el| yield }
end
